import React, { Component } from 'react';
import HeaderInformation from './components/HeaderInformation';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <HeaderInformation></HeaderInformation>
      </div>
    );
  }
}

export default App;
